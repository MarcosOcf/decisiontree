import math
import random as rd
from enum import Enum
from decisiontree.params import AttributeType

class NodeType(Enum):
    NODE_NULL = 1
    NODE_LEAF = 2
    NODE_INTERNAL = 3

class Tree:
    def __init__(self, params):
        self.params = params
        self.nodes = [Node(params) for x in range(2**(params.max_depth+1)-1) ]
        for i in range(0,len(self.nodes)):
            self.nodes[i] = Node(params)

        self.nodes[0].node_type = NodeType.NODE_LEAF

        for i in range(0,params.nb_samples):
            self.nodes[0].add_sample(i)
        self.nodes[0].evaluate()

    def print(self):
        nbMisclassifiedSamples = 0
        print("---------------------------------------- PRINTING SOLUTION ----------------------------------------")
        serialized = ""
        for d in range(0, self.params.max_depth+1):
            inicio = 2**d - 1
            fim = 2**(d+1) - 1
            for i in range(inicio,fim):
                if (self.nodes[i].node_type == NodeType.NODE_INTERNAL):
                    sinal = "="
                    if (self.params.attribute_types[self.nodes[i].split_attribute] == AttributeType.TYPE_NUMERICAL):
                        sinal = "<="
                    serialized = serialized + "(Node"+ str(i) + ", Attribute["+ str(self.nodes[i].split_attribute) + "]"+ sinal + str(self.nodes[i].split_value)+ ")"
                else:
                    if (self.nodes[i].node_type == NodeType.NODE_LEAF):
                        misclass = self.nodes[i].nb_samples_node - self.nodes[i].nb_samples_class[self.nodes[i].majorityClass]
                        nbMisclassifiedSamples += misclass
                        serialized = serialized + "(Leave" + str(i) + ", Majority_Class" + str(self.nodes[i].majorityClass) + "," + str(self.nodes[i].nb_samples_class[self.nodes[i].majorityClass]) + "," + str(misclass) + ") "
            serialized = serialized + "\n"
        print (serialized)
        print(nbMisclassifiedSamples , "/" , self.params.nb_samples , " MISCLASSIFIED SAMPLES" )
        print("---------------------------------------------------------------------------------------------------" )

        print("NB_SAMPLES: " ,  self.params.nb_samples )
        print("NB_MISCLASSIFIED: " , nbMisclassifiedSamples)

    def missclassifieds(self):
        nbMisclassifiedSamples = 0
        for d in range(0, self.params.max_depth+1):
            inicio = 2**d - 1
            fim = 2**(d+1) - 1
            for i in range(inicio,fim):
                if (self.nodes[i].node_type == NodeType.NODE_INTERNAL):
                    pass
                else:
                    if (self.nodes[i].node_type == NodeType.NODE_LEAF):
                        misclass = self.nodes[i].nb_samples_node - self.nodes[i].nb_samples_class[self.nodes[i].majorityClass]
                        nbMisclassifiedSamples += misclass

        self._missclassifieds = nbMisclassifiedSamples
        return self._missclassifieds


class Node:
    def __init__(self, params):
        self.params = params
        self.node_type = NodeType.NODE_NULL     # Node type
        self.params = params                   # Access to the problem and dataset parameters
        self.split_attribute = -1               # Attribute to which the split is applied (filled through the greedy algorithm)
        self.split_value = -1.e30               # Threshold value for the split (for numerical attributes the left branch will be <= split_value, for categorical will be == split_value)
        self.samples = []                      # Samples from the training set at this node
        self.nb_samples_class = [0 for x in range(self.params.nb_classes+1)] # Number of samples of each class at this node (for each class)
        self.nb_samples_node = 0                 # Total number of samples in this node
        self.majorityClass = -1                # Majority class in this node
        self.max_same_class = 0                  # Maximum number of elements of the same class in this node
        self.entropy = -1.e30                  # Entropy in this node
        self.split_history = []

    def evaluate(self):
        self.entropy = 0
        for c in range(0,self.params.nb_classes):
            if (self.nb_samples_class[c]>0):
                frac = self.nb_samples_class[c]/self.nb_samples_node
                self.entropy -= frac * math.log(frac)
                if (self.nb_samples_class[c] > self.max_same_class):
                    self.max_same_class = self.nb_samples_class[c]
                    self.majorityClass = c

    def add_sample(self, i):
        self.samples.append(i)
        self.nb_samples_class[int(self.params.data_classes[i])] += 1;
        self.nb_samples_node += 1
