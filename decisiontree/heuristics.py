from random import randint
from decisiontree.split_methods import Greedy
from decisiontree.decorators import with_runtime, with_profile
from decisiontree.tree import Tree
from decisiontree.helpers import tweak
from datetime import datetime

@with_runtime
def cart(params, split_method=Greedy):
    tree = Tree(params)
    split_method(params, tree)
    return tree


@with_runtime
def hill_climbing(params, split_method=Greedy, timeout=10):
    best = Tree(params)
    split_method(params, best)

    start = datetime.now()

    while best.missclassifieds() != 0:
        if (datetime.now() - start).seconds > timeout:
            break

        decisions = tweak(best)
        new_tree = Tree(params)
        split_method(params, tree=new_tree, forced_decisions=decisions)

        print('best {}, miss {}'.format(best.missclassifieds(), new_tree.missclassifieds()))

        if (best.missclassifieds() >= new_tree.missclassifieds()):
            best = new_tree

    return best
