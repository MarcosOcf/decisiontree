import cProfile
from datetime import datetime

def with_runtime(function):
    def wrapper(params, split_method):
        startTime = datetime.now()
        output = function(params, split_method)
        endTime = datetime.now()

        print("TIME(s): ", (endTime - startTime))
        return output

    return wrapper

def with_profile(function):
    def wrapper(params, split_method):
        pr = cProfile.Profile()
        pr.enable()

        output = function(params, split_method)

        pr.disable()
        pr.print_stats()

        return output

    return wrapper
