from copy import copy
from random import randint

def tweak(tree):
    new_decisions = dict()
    tries = 0

    while tries < 5:
        node = randint(0, 2**(tree.params.max_depth) - 2)

        if len(tree.nodes[node].split_history) > 0:
            new_decision = randint(0, max(len(tree.nodes[node].split_history) - 2, 0))
            new_decisions[node] = tree.nodes[node].split_history[new_decision]

            break

        tries +=1

    return new_decisions
