import math
import random as rd
from enum import Enum
from decisiontree.params import AttributeType
from decisiontree.tree import NodeType


class Greedy:
    def __init__(self, params, tree, forced_decisions=None):
        self.params = params
        self.forced_decisions = forced_decisions
        self.tree = tree
        self.recursive_construction(0, 0)

    def recursive_construction(self, node, level):
        node_obj = self.tree.nodes[node]

        # BASE CASES -- MAXIMUM LEVEL HAS BEEN ATTAINED OR ALL SAMPLES BELONG TO THE SAME CLASS
        if (( level >= self.params.max_depth ) or ( node_obj.max_same_class == node_obj.nb_samples_node )):
            return


#         LOOK FOR A BEST SPLIT
        all_identical = True
        nb_samples_node = node_obj.nb_samples_node
        original_entropy = node_obj.entropy
        best_information_gain = -1.e30
        best_split_attribute = -1
        best_split_threhold = -1.e30
        MY_EPSILON=0.00001
        split_history = []

        if self.forced_decisions is not None and node in self.forced_decisions:
            best_split_attribute = self.forced_decisions[node]['split_attribute']
            best_split_threhold = self.forced_decisions[node]['split_threshold']
        else:
            for att in range(0, self.params.nb_attributes):
                if (self.params.attribute_types[att] == AttributeType.TYPE_NUMERICAL):
    #                 CASE 1) -- FIND SPLIT WITH BEST INFORMATION GAIN FOR NUMERICAL ATTRIBUTE c */

    #                 Define some data structures
                    ordered_samples = []        #Order of the samples according to attribute c
                    attribute_levels = []       #Store the possible levels of this attribute among the samples (will allow to "skip" samples with equal attribute value)
                    for s in node_obj.samples:
                        ordered_samples.append((self.params.data_attributes[s][att],int(self.params.data_classes[s])))
                        attribute_levels.append(self.params.data_attributes[s][att])
                    attribute_levels.sort()
                    ordered_samples.sort()
                    if (len(attribute_levels)<=1):
                        continue
                    else:
                        all_identical = False

                    #Initially all samples are on the right

                    nb_samples_class_left = [0 for y in range(self.params.nb_classes+1)]
                    nb_samples_class_right = [node_obj.nb_samples_class[y] for y in range(self.params.nb_classes+1)]
                    index_sample = 0

                    # Go through all possible attribute values in increasing order
                    # Iterate on all samples with this attributeValue and switch them to the left
                    for attributeValue in attribute_levels:

                        while (index_sample < node_obj.nb_samples_node and ordered_samples[index_sample][0] < float(attributeValue + MY_EPSILON)):
                            nb_samples_class_left[ordered_samples[index_sample][1]]  += 1
                            nb_samples_class_right[ordered_samples[index_sample][1]] -= 1
                            index_sample += 1

                        if (index_sample != nb_samples_node):
                            #Evaluate entropy of the two resulting sample sets
                            entropy_left = 0
                            entropy_right = 0

                            for c in range(0, self.params.nb_classes):
                                #Remark that index_sample contains at this stage the number of samples in the left
                                if (nb_samples_class_left[c]>0):
                                    frac_left = nb_samples_class_left[c] / index_sample
                                    entropy_left -= frac_left * math.log(frac_left)
                                if (nb_samples_class_right[c]>0):
                                    frac_right = nb_samples_class_right[c] / (node_obj.nb_samples_node - index_sample)
                                    entropy_right -= frac_right * math.log(frac_right)

                                #Evaluate the information gain and store if this is the best option found until now
                            information_gain = original_entropy - (index_sample*entropy_left + (nb_samples_node - index_sample)*entropy_right) / nb_samples_node

                            if (information_gain >= best_information_gain):
                                if information_gain > best_information_gain:
                                    best_information_gain = information_gain
                                    best_split_attribute = att
                                    best_split_threhold = attributeValue

                                if (
                                    not split_history or
                                    (split_history[-1]['split_attribute'] != att and
                                    split_history[-1]['split_threshold'] != attributeValue)
                                ):
                                    split_history.append(dict(
                                        split_attribute=att,
                                        split_threshold=attributeValue,
                                        information_gain=information_gain
                                    ))
                else:
                    #CASE 2) -- FIND BEST SPLIT FOR CATEGORICAL ATTRIBUTE c
                    #Count for each level of attribute c and each class the number of samples
                    nb_samples_level = [0 for y in range(int(self.params.nb_levels[att]))]
                    nb_samples_class = [0 for y in range(self.params.nb_classes)]
                    nb_samples_levelClass = {}

                    for k in range(int(self.params.nb_levels[att])):
                        nb_samples_levelClass[k] = [0 for i in range(self.params.nb_classes)]

                    for s in node_obj.samples:
                        s = int(s)
                        att = int(att)

                        nb_samples_level[int(self.params.data_attributes[s][att])]+=1
                        nb_samples_class[int(self.params.data_classes[s])]+=1

                        #print(int(self.params.data_attributes[s][att]))
                        #print(self.params.data_classes[s])
                        #print(nb_samples_levelClass)

                        nb_samples_levelClass[int(self.params.data_attributes[s][att])][int(self.params.data_classes[s])] = int(nb_samples_levelClass[int(self.params.data_attributes[s][att])][int(self.params.data_classes[s])])+1
                    #print(self.params.nb_levels[att])
                    #Calculate information gain for a split at each possible level of attribute c
                    for l in range(0,int(self.params.nb_levels[att])):
                        if (nb_samples_level[l] > 0 and nb_samples_level[l] < nb_samples_node):
                            #Evaluate entropy of the two resulting sample sets
                            all_identical = False
                            entropy_level = 0
                            entropyOthers = 0
                            for c in range(0,self.params.nb_classes):
                                if (nb_samples_levelClass[l][c] > 0):
                                    fracLevel = nb_samples_levelClass[l][c] / nb_samples_level[l]
                                    entropy_level -= fracLevel * math.log(fracLevel)
                                if (nb_samples_class[c] - nb_samples_levelClass[l][c] > 0):
                                    fracOthers = (nb_samples_class[c] - nb_samples_levelClass[l][c]) / (nb_samples_node - nb_samples_level[l])
                                    entropyOthers -= fracOthers * math.log(fracOthers)
                            # Evaluate the information gain and store if this is the best option found until now
                            information_gain = original_entropy - (nb_samples_level[l] *entropy_level + (nb_samples_node - nb_samples_level[l])*entropyOthers) / nb_samples_node
                            if (information_gain >= best_information_gain):
                                if information_gain > best_information_gain:
                                    best_information_gain = information_gain
                                    best_split_attribute = att
                                    best_split_threhold = l

                                if (
                                    not split_history or
                                    (split_history[-1]['split_attribute'] != att and
                                    split_history[-1]['split_threshold'] != l)
                                ):
                                    split_history.append(dict(
                                        split_attribute=att,
                                        split_threshold=l,
                                        information_gain=information_gain
                                    ))
            # SPECIAL CASE TO HANDLE POSSIBLE CONTADICTIONS IN THE DATA
            # (Situations where the same samples have different classes -- In this case no improving split can be found)

        if (all_identical):
            return

        # APPLY THE SPLIT AND RECURSIVE CALL */
        node_obj.split_attribute = best_split_attribute
        node_obj.split_value = best_split_threhold
        node_obj.node_type = NodeType.NODE_INTERNAL
        node_obj.split_history = split_history


        self.tree.nodes[2*node+1].node_type = NodeType.NODE_LEAF
        self.tree.nodes[2*node+2].node_type = NodeType.NODE_LEAF
        for s in node_obj.samples:
            if ((self.params.attribute_types[best_split_attribute] == AttributeType.TYPE_NUMERICAL and
                 self.params.data_attributes[s][best_split_attribute] < best_split_threhold + MY_EPSILON) or
                (self.params.attribute_types[best_split_attribute] == AttributeType.TYPE_CATEGORICAL and
                 self.params.data_attributes[s][best_split_attribute] < best_split_threhold + MY_EPSILON and
                 self.params.data_attributes[s][best_split_attribute] > best_split_threhold - MY_EPSILON)):
                self.tree.nodes[2*node+1].add_sample(s)
            else:
                self.tree.nodes[2*node+2].add_sample(s)
        self.tree.nodes[2*node+1].evaluate() # Setting all other data structures
        self.tree.nodes[2*node+2].evaluate() # Setting all other data structures
        self.recursive_construction(2*node+1,level+1) # Recursive call
        self.recursive_construction(2*node+2,level+1) # Recursive call
