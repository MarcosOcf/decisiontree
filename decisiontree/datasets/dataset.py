import numpy as np
import os.path

current_path = os.path.abspath(os.path.dirname(__file__))

def read_file(file):
    f = open(file, 'r')
    counter = 0
    array = []

    for line in f.readlines():
        if (counter == 3):
            column_types = line.rstrip().split(' ')[1:]
        if (counter >= 5) and 'EOF' not in line:
            try:
                array.append(list(map(float, line.rstrip().split(' '))))
            except:
                print(line)

        counter += 1

    dataset = np.array(array)
    class_values = np.unique(dataset[:, -1])
    return dict(dataset=np.array(array), column_types=column_types, class_values=class_values)


def get_dataset(path):
    return read_file(os.path.join(current_path, path))


sample = dict(
    dataset=np.array(
        [[2.771244718,1.784783929,0],
    	[1.728571309,1.169761413,0],
    	[3.678319846,2.81281357,0],
    	[3.961043357,2.61995032,0],
    	[2.999208922,2.209014212,0],
    	[7.497545867,3.162953546,1],
    	[9.00220326,3.339047188,1],
    	[7.444542326,0.476683375,1],
    	[10.12493903,3.234550982,1],
    	[6.642287351,3.319983761,1]]),
    column_types=['N', 'N', 'C'],
    class_values=[0, 1]
)


p01 = get_dataset('./p01.txt')
p02 = get_dataset('./p02.txt')
p03 = get_dataset('./p03.txt')
p04 = get_dataset('./p04.txt')
p05 = get_dataset('./p05.txt')
p06 = get_dataset('./p06.txt')
p07 = get_dataset('./p07.txt')
p08 = get_dataset('./p08.txt')
p09 = get_dataset('./p09.txt')
p10 = get_dataset('./p10.txt')
