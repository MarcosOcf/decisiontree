import math
import random as rd
from enum import Enum

class AttributeType(Enum):
    TYPE_NUMERICAL = 1
    TYPE_CATEGORICAL = 2

class Params(object):
    def __init__(self, pathToInstance, pathToSolution, seedRNG, max_depth, maxTime):
        rd.seed(seedRNG)
        print("INITIALIZING RNG WITH SEED: ",seedRNG)
        f=open(pathToInstance, "r")
        self.max_depth = max_depth-1

        attribute_types = []

        line = f.readline()
        self.dataset_name = line.replace("\n","").split(" ")[1]
        line = f.readline()
        self.nb_samples = int(line.split(" ")[1])
        line = f.readline()
        self.nb_attributes = int(line.split(" ")[1])
        line = f.readline()
        self.att_type = line.replace("\n","").split(" ")

        line = f.readline()
        self.nb_classes = int(line.split(" ")[1])
        self.attribute_types = []
        for i in range(1,len(self.att_type)):
            if(self.att_type[i] == "C"):
                self.attribute_types.append(AttributeType.TYPE_CATEGORICAL)
            else:
                if (self.att_type[i] == "N"):
                    self.attribute_types.append(AttributeType.TYPE_NUMERICAL)
                else:
                    print("ERROR: non recognized attribute type", self.att_type)
        self.data_attributes = [[0 for x in range(int(self.nb_attributes))] for y in range(int(self.nb_samples))]
        self.data_classes = [0 for y in range(int(self.nb_samples))]
        self.nb_levels =  [0 for y in range(int(self.nb_attributes))]

        for s in range(0,self.nb_samples):
            line = f.readline()
            self.attributes = [float(y) for y in line.split(" ")]
            for i in range(0,self.nb_attributes):
                self.data_attributes[s][i] = self.attributes[i]
                if ((self.attribute_types[i] == AttributeType.TYPE_CATEGORICAL)
                    and (self.data_attributes[s][i]+1 > self.nb_levels[i])):
                    self.nb_levels[i] = self.data_attributes[s][i]+1
            self.data_classes[s]=self.attributes[self.nb_attributes]
        f.close()

        print("----- DATASET [",self.dataset_name,"]") # LOADED IN " << clock()/ (double)CLOCKS_PER_SEC << "(s)" << std::endl;
        print("----- NUMBER OF SAMPLES: ", self.nb_samples)
        print("----- NUMBER OF ATTRIBUTES: ",self.nb_attributes)
        print("----- NUMBER OF CLASSES: ",self.nb_classes)
