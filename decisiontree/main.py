import argparse
from decisiontree.split_methods import Greedy
from decisiontree.heuristics import cart, hill_climbing
from decisiontree.params import Params

params = ['p01', 'p02', 'p03', 'p04', 'p05', 'p06', 'p07', 'p08', 'p09', 'p10']
methods = dict(cart=cart, hill_climbing=hill_climbing)
split_methods=dict(greedy=Greedy)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset', default='p10', type=str, choices=params)
    parser.add_argument('-m', '--method', default='cart', type=str, choices=methods.keys())
    parser.add_argument('-s', '--split-method', default='greedy', choices=split_methods.keys())

    return parser.parse_args()


def build_execution(args):
    params = Params("./decisiontree/datasets/{}.txt".format(args.dataset),"",30,4,5)
    split_method = split_methods[args.split_method]
    function = methods[args.method]

    return lambda: function(params=params, split_method=split_method)


if __name__ == "__main__":
    exec = build_execution(get_args())
    tree = exec()
    tree.print()
