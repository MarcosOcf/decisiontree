from setuptools import setup, find_packages
setup(
    name='decision_tree',
    version='0.0.1',
    packages=find_packages('.'),
    url='',
    license='',
    author='Marcos Couto',
    author_email='',
    description='',
    install_requires=[
        "numpy==1.16.2"
    ]
)
